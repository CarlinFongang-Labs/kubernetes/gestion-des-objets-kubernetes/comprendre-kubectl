# Comprendre kubectl

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______ 

# Objectifs

Nous abordons dans ce lab l'utilisation de kubectl. 
Aperçu des sujets traités :

- Qu'est-ce que kubectl ?
- La commande kubectl get
- La commande kubectl describe
- La commande kubectl create
- La commande kubectl apply
- La commande kubectl delete
- La commande kubectl exec
- Démo


# 1. Qu'est-ce que kubectl ?
Kubectl est l'outil en ligne de commande permettant d'interagir avec Kubernetes. Il utilise l'API Kubernetes pour communiquer avec le cluster et exécuter des commandes. Selon la documentation Kubernetes, kubectl permet de déployer des applications, d'inspecter et de gérer les ressources du cluster, et de visualiser les journaux.

# 2. La commande **"kubectl get"**

**"Kubectl get"** permet de lister les objets dans le cluster Kubernetes. 
Pour l'utiliser, il faut spécifier un type d'objet, par exemple **kubectl get pods** pour obtenir une liste des **pods**. Il est également possible de fournir le nom d'un objet pour obtenir des informations spécifiques. Des options supplémentaires sont disponibles :

**-o** pour définir le format de sortie (par exemple yaml ou json).
**--sort-by** pour trier la sortie en utilisant une expression json path.
**--selector** pour filtrer les résultats par labels spécifiques.

Syntaxe :

```bash
$ kubectl get <object type> <objet name> -o <>output --sort-by <JSONPath> --selector <selector>
```




# 3. La commande **kubectl describe**

**Kubectl describe** permet d'obtenir des informations détaillées sur des objets Kubernetes spécifiques. La commande kubectl describe suivie du type d'objet et du nom de l'objet fournit des détails complets.

Syntaxe :

```bash
$ kubectl describe <object type> <object name>
```

# 4. La commande **kubectl create**

**"Kubectl create"** permet de créer des objets. On peut utiliser des commandes impératives ou la commande avec l'option **"-f"** pour créer un objet à partir d'un fichier de description **yaml**. Si un objet avec le même nom et le même type existe déjà dans le **namespace** en cours, une erreur est générée.

Syntaxe :

```bash
$ kubectl crate -f  <file name>
```

# 5. La commande kubectl apply
**"Kubectl apply"** est similaire à kubectl create, mais s'il est utilisé sur un objet existant, il tente de le modifier au lieu de générer une erreur. Cela permet de créer un nouvel objet ou de mettre à jour un objet existant.

Syntaxe :

```bash
$ kubectl apply -f  <file name>
```

# 4. La commande kubectl delete
**"Kubectl delete"** est utilisée pour supprimer des objets du cluster. C'est une commande simple et directe pour la gestion des objets.

Syntaxe :

```bash
$ kubectl delete <object type> <object name>
```

# 5. La commande kubectl exec
**"Kubectl exec"** permet d'exécuter des commandes à l'intérieur des conteneurs. C'est très utile pour le dépannage et l'exploration de ce qui se passe à l'intérieur des conteneurs. Pour que la commande réussisse, le conteneur doit contenir le logiciel nécessaire pour exécuter la commande.

Syntaxe :

```bash
$ kubectl exec <pod name> -c  <container name> -- <command>
```

# 5. Démonstration pratique

[Consultez le lab relative à cette demo en suivant ce lien](https://gitlab.com/CarlinFongang-Labs/kubernetes/gestion-des-objets-kubernetes/lab-utiliser-kubctl-dans-un-cluster-kubernetes.git)

# Conclusion
Nous avons couvert les bases de kubectl et plusieurs de ses commandes importantes, y compris kubectl get, describe, create, apply, delete et exec. La démonstration pratique a permis d'explorer ces commandes.


# Reference 

https://kubernetes.io/docs/reference/kubectl/